import { Guild, GuildMember, MessageEmbed, Role } from "discord.js";
import { Client, Command, CommandoMessage } from "discord.js-commando";


module.exports = class ListCommand extends Command {
    constructor (client: Client) {
        super(client, {
            name: 'list',
            group: 'clan',
            memberName: 'list',
            description: 'List inactives with clan role',
            args: [
                {
                    key: 'clanRole',
                    prompt: 'What clan do you need inactives from?',
                    type: 'role'
                }
            ]
        })
    }
    async run (message: CommandoMessage): Promise<CommandoMessage> {
        const guild: Guild = message.guild ?? null
        const clanRole: Role = message.mentions.roles.first() as Role
        const clanMembers: GuildMember[] = Array.from(clanRole.members.values())

        const embed: MessageEmbed = new MessageEmbed().setColor(clanRole.color)
        const inactives: GuildMember[] = new Array<GuildMember>()

        for (const mem of inactives) {
            if (mem.roles.cache.has(clanRole.toString()) && !mem.roles.cache.has('Active')) {
                embed.addField(`${mem.nickname}`, `${Array.from(mem.roles.cache.values())}`)
            }
        }

        return await message.say(embed)
    }
}