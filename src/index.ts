import { Client } from 'discord.js-commando'
import path from 'path'

const client: Client = new Client({
    owner: '191999414817128449',
    commandPrefix: 'l.'
})

try {
    client.registry
    .registerDefaults()
    .registerGroups([
        ['clan', 'clan management commands']
    ])
    .registerCommandsIn({
        filter: /^[^.].*\.(js|ts)$/,
        dirname: path.join(__dirname, 'commands')
    })
} catch (err) {
    console.error(err)
}

client.once('ready', () => {
    console.log(`logged in as ${client.user?.tag ?? ''}`)
    client.user?.setActivity('with Hawthorne')
})

client.on('error', console.error)

client.login(process.env.DISCORD_TOKEN).catch(console.error)